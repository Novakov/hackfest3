﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using System.Configuration;

namespace Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new IdentityFilter());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "CalendarViewDate",
                "Calendar/{year}/{month}/{day}",
                new
                {
                    controller = "Calendar", action = "ViewCalendar"
                }
            );

            routes.MapRoute(
                "iCalFile",
                "Calendar/{id}.ics",
                new
                {
                    controller = "Calendar", action = "GetCalendarFileByToken"
                }
            );

            routes.MapRoute(
                "CalendarViewToday",
                "Calendar",
                new
                {
                    controller = "Calendar", action = "ViewCalendar"
                }
            );

            routes.MapRoute(
                "Guide",
                "GettingStarted",
                new
                {
                    controller = "GettingStarted",
                    action = "Index"
                }
            );

            routes.MapRoute(
                "Start", // Route name
                "", // URL with parameters
                new
                {
                    controller = "Home", action = "Index", id = UrlParameter.Optional
                } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Home", action = "Index", id = UrlParameter.Optional
                } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            RegisterDI();

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        private void RegisterDI()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["storage"].ConnectionString;

            var containerBuilder = new ContainerBuilder();
            
            containerBuilder.RegisterModule<MvcModule>();
            containerBuilder.RegisterModule(new DataAccess.DI.NHibernateModule(connectionString, Model.Marker.Assembly));
            containerBuilder.RegisterModule(new DataAccess.DI.CqrsModule(Logic.Marker.Assembly));

            var container = containerBuilder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}