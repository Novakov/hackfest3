﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace Web.ViewBase
{
    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public BaseViewPage()
        {
            Sidebar = null;
        }

        public string Sidebar
        {
            get
            {
                return ViewBag.Sidebar;
            }
            set
            {
                ViewBag.Sidebar = value;
            }
        }

        public object Id(int id)
        {
            return new
            {
                id = id
            };
        }

        public object Id(string id)
        {
            return new
            {
                id = id
            };
        }

        public AjaxOptions AjaxOptions(string onSuccess = null, string onError = null)
        {
            return new AjaxOptions
            {
                OnSuccess = onSuccess,
                OnFailure = onError
            };
        }
    }

    public abstract class BaseViewPage : BaseViewPage<dynamic>
    {
    }
}