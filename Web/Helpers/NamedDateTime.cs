﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Web.Helpers
{
    public class NamedDateTime
    {
        internal NamedDateTime(DateTime dateTime, string name)
        {
            this.Value = dateTime;
            this.Name = name;
        }

        public DateTime Value
        {
            get;
            private set;
        }

        public DateTime Date
        {
            get
            {
                return Value.Date;
            }            
        }

        public string Name
        {
            get;
            private set;
        }
    }
}
