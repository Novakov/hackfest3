﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Helpers
{
    public static class EnumerableHelper
    {
        public static IEnumerable<IndexedValue<T>> Indexed<T>(this IEnumerable<T> @this)
        {
            return @this.Select((x, i) => new IndexedValue<T>(x, i));
        }

        public class IndexedValue<T>
        {
            public IndexedValue(T value, int index)
            {
                this.Value = value;
                this.Index = index;
            }

            public int Index
            {
                get;
                set;
            }

            public T Value
            {
                get;
                set;
            }
        }
    }
}