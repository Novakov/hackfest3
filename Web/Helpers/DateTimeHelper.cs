﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Helpers
{
    public static class DateTimeHelper
    {
        public static IEnumerable<NamedDateTime> AsNamed(this IEnumerable<DateTime> @this, Func<DateTime, string> name)
        {
            return @this.Select(x => new NamedDateTime(x, name(x)));
        }

        public static DateTime TranslateToTimeZone(this DateTime @this, string timeZoneId)
        {
            return @this.TranslateToTimeZone(TimeZoneInfo.FindSystemTimeZoneById(timeZoneId));
        }

        public static DateTime TranslateToTimeZone(this DateTime @this, TimeZoneInfo timezone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(@this.ToUniversalTime(), timezone);
        }
    }
}