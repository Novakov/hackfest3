﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using System.IO;
using System.Text;
using Logic.CalendarFileGenerator;
using DataAccess;
using System.Security.Cryptography;

namespace Web.Controllers
{
    [Authorize]
    public class CalendarController : ControllerBase
    {        
        private IScopeFactory scopeFactory;

        public CalendarController(IScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        public ActionResult ViewCalendar(int? year, int? month, int? day)
        {
            DateTime showDay;
            if(year.HasValue && month.HasValue && day.HasValue)
            {
                showDay = new DateTime(year.Value, month.Value, day.Value);
            }
            else
            {
                showDay = DateTime.Today;
            }

            var week = CalendarHelper.GetWeekRange(showDay);

            var prevWeek = week.StartDate.AddDays(-1);
            var nextWeek = week.EndDate.AddDays(1);

            ViewData.Model = new ViewModels.Calendar.View
            {
                Week = week,                
                Days = CalendarHelper.GetWeekDays(showDay).ToArray(),
                Hours = Enumerable.Range(7, 11).ToArray(),                
                PrevWeekUrl = Url.Action("ViewCalendar", DateRoute(prevWeek)),
                NextWeekUrl = Url.Action("ViewCalendar", DateRoute(nextWeek)),
            };

            if(Request.IsAjaxRequest())
            {
                return PartialView("_Calendar");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetCalendarFile()
        {
            try
            {
                CalendarFileGenerator generator = new CalendarFileGenerator(User.Id);
                string data = generator.GetICalString(scopeFactory);

                // convert string to stream
                byte[] byteArray = Encoding.ASCII.GetBytes(data);
                MemoryStream stream = new MemoryStream(byteArray);

                return new FileStreamResult(stream, "application/txt") { FileDownloadName = "Calendar.ics" };
            }
            catch (NullReferenceException)
            {
                return View();
            }
        }

        public ActionResult GetCalendarFileByToken(string id)
        {
            try
            {
                Model.User u = scopeFactory.Do(x => x.Query(new Logic.User.GetUserByCalendarToken(id)));

                // generate isc file for user
                CalendarFileGenerator generator = new CalendarFileGenerator(u.Id);
                string data = generator.GetICalString(scopeFactory);

                // convert string to stream
                byte[] byteArray = Encoding.ASCII.GetBytes(data);
                MemoryStream stream = new MemoryStream(byteArray);

                return new FileStreamResult(stream, "application/txt") { FileDownloadName = "Calendar.ics" };
            }
            catch(NullReferenceException)
            {
                return new HttpNotFoundResult();
            }
        }

        private object DateRoute(DateTime day)
        {
            return new
            {
                year = day.Year,
                month = day.Month,
                day = day.Day
            };
        }
    }
}
