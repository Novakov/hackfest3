﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public abstract class ControllerBase : Controller
    {
        public new Model.User User
        {
            get
            {
                var userid = HttpContext.User.Identity as Logic.UserIdentity;

                if (userid != null)
                {
                    return userid.User;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}