﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;

namespace Web.Controllers
{
    [Authorize]
    public class TaskController : ControllerBase
    {
        private IScopeFactory scopeFactory;

        public TaskController(IScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        public ActionResult Create()
        {
            ViewData.Model = new ViewModels.Task.Create
            {
                AvailableDurations = GetAvailableDurations().ToArray(),
                Task = new Model.Task()
            };

            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(ViewModels.Task.Create model)
        {
            if(ModelState.IsValid)
            {
                if(User != null)
                {
                    model.Task.User = User;
                }
                else
                {
                    throw new InvalidOperationException("You must be logged in to create tasks.");
                }

                model.Task.Project = null;
                model.Task.Visible = true;
                int id = scopeFactory.Do(x => x.Execute(new Logic.Task.CreateTask(model.Task)));

                return Json("OK");
            }

            model.AvailableDurations = GetAvailableDurations().ToArray();

            ViewData.Model = model;

            Response.StatusCode = 510;
            return PartialView();
        }

        public ActionResult Edit(int id)
        {
            var entity = scopeFactory.Do(x => x.Query(new Logic.Task.GetTaskById(id)));
            ViewData.Model = new ViewModels.Task.Edit
            {
                AvailableDurations = GetAvailableDurations().ToArray(),
                Description = entity.Description,
                Duration = entity.Duration,
                Id = entity.Id,
                Name = entity.Name,
                Visible = entity.Visible
            };

            return PartialView();
        }

        [HttpPost]
        public ActionResult Edit(ViewModels.Task.Edit model)
        {
            if(ModelState.IsValid)
            {                                
                using(var scope = scopeFactory.Get())
                {
                    var entity = scope.Query(new Logic.Task.GetTaskById(model.Id));

                    entity.Description = model.Description;
                    entity.Duration = model.Duration;
                    entity.Name = model.Name;
                    entity.Visible = model.Visible;

                    scope.Execute(new Logic.Task.UpdateTask(entity));

                    scope.Commit();
                }

                return Json("OK");
            }


            model.AvailableDurations = GetAvailableDurations().ToArray();
            ViewData.Model = model;

            Response.StatusCode = 510;
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            scopeFactory.Do(x => x.Execute(new Logic.Task.DeleteTask(id)));

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAvailable()
        {
            var tasks = scopeFactory.Do(x => x.Query(new Logic.Task.GetTasksForUser(User.Id)));

            return Json(tasks.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                duration = (int) x.Duration.TotalHours
            }), JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<Tuple<TimeSpan, string>> GetAvailableDurations()
        {
            return from d in Enumerable.Range(1, 11)
                   let unit = d == 1 ? "hour" : "hours"
                   let ts = TimeSpan.FromHours(d)
                   let text = string.Format("{0} {1}", d, unit)
                   select Tuple.Create(ts, text);
        }
    }
}
