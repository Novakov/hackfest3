﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Web.Models;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DataAccess;
using System.Security.Cryptography;

namespace Web.Controllers
{
    public class AccountController : ControllerBase
    {
        private IScopeFactory scopeFactory;
        private OpenIdRelyingParty openid;

        public AccountController(IScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;

            this.openid = new OpenIdRelyingParty();
        }

        public ActionResult LogOn(string returnUrl)
        {
            var response = openid.GetResponse();

            if(response == null)
            {
                return StartLogOn(returnUrl);
            }
            else
            {
                return ProceedLogOn(returnUrl, response);
            }
        }

        private ActionResult ProceedLogOn(string returnUrl, IAuthenticationResponse response)
        {
            switch(response.Status)
            {
                case AuthenticationStatus.Authenticated:
                    LogOnModel lm = new LogOnModel();
                    lm.OpenID = response.ClaimedIdentifier;

                    // check if user exist

                    if(IsExistingOpenId(lm.OpenID))
                    {
                        FormsAuthentication.SetAuthCookie(lm.OpenID, false);

                        if(string.IsNullOrWhiteSpace(returnUrl))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            return Redirect(returnUrl);
                        }
                    }

                    var sreg = response.GetExtension<ClaimsResponse>();

                    Session["claims"] = sreg;

                    return RedirectToAction("Register", new
                    {
                        openId = lm.OpenID
                    });

                case AuthenticationStatus.Canceled:
                    ViewBag.Message = "Canceled at provider";
                    return View("LogOn");
                case AuthenticationStatus.Failed:
                    ViewBag.Message = response.Exception.Message;
                    return View("LogOn");
            }

            return new EmptyResult();
        }

        private bool IsExistingOpenId(string token)
        {
            return scopeFactory.Do(x => x.Query(new Logic.User.ValidateOpenId(token)));
        }

        private ActionResult StartLogOn(string returnUrl)
        {
            //Let us submit the request to OpenID provider
            Identifier id;
            if(Identifier.TryParse(Request.Form["openid_identifier"], out id))
            {
                try
                {
                    var request = openid.CreateRequest(Request.Form["openid_identifier"]);

                    request.AddExtension(new ClaimsRequest
                    {
                        Email = DemandLevel.Require,
                        FullName = DemandLevel.Require,
                        Nickname = DemandLevel.Require,

                    });

                    return request.RedirectingResponse.AsActionResult();
                }
                catch(ProtocolException ex)
                {
                    ViewBag.Message = ex.Message;
                    return View("LogOn");
                }
            }

            return View("LogOn");
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register(string openId)
        {
            var sreg = (ClaimsResponse) Session["claims"];
            //Session["claims"] = null;

            ViewData.Model = new Models.RegisterModel
            {
                Email = sreg.Email,
                DisplayName = sreg.FullName ?? sreg.Nickname,
                OpenID = openId
            };
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if(ModelState.IsValid)
            {
                scopeFactory.Do(x => x.Execute(new Logic.User.RegisterUser(model.Email, model.DisplayName, model.OpenID, model.TimeZone)));

                FormsAuthentication.SetAuthCookie(model.OpenID, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        public ActionResult GenerateNewToken()
        {
            string source = User.Mail + DateTime.Now.ToString();
            string hash;

            using (MD5 md5Hash = MD5.Create())
            {
                hash = Common.MD5Helper.GetMd5Hash(md5Hash, source);
            }

            // save to db
            User.CalendarToken = hash;
            scopeFactory.Do(x => x.Execute(new Logic.User.UpdateUser(User)));

            return Json(new { link = Url.Action("GetCalendarFileByToken", "Calendar", new { id = hash }, "http") }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Manage()
        {
            ViewData.Model = User;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Manage(Model.User model)
        {
            if (ModelState.IsValid)
            {
                User.Mail = model.Mail;
                User.Name = model.Name;
                User.TimeZone = model.TimeZone;

                scopeFactory.Do(x => x.Execute(new Logic.User.UpdateUser(User)));
            }

            ViewData.Model = User;
            return View();
        }
    }
}
