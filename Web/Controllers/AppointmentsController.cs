﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using Common;
using Web.Helpers;

namespace Web.Controllers
{
    [Authorize]
    public class AppointmentsController : ControllerBase
    {
        private IScopeFactory scopeFactory;

        public AppointmentsController(IScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        [HttpPost]
        public ActionResult Create(ViewModels.Appointments.Create model)
        {
            var entity = new Model.Appointment
            {
                Task = new Model.Task
                {
                    Id = model.TaskId
                },

                StartDate = model.Date.AddHours(model.StartHour),
                EndDate = model.Date.AddHours(model.EndHour).AddMinutes(59).AddSeconds(59)
            };

            scopeFactory.Do(x => x.Execute(new Logic.Appointment.CreateAppointment(entity)));
            return Json(new
            {
                id = entity.Id
            });
        }

        [HttpPost]
        public ActionResult Update(ViewModels.Appointments.Update model)
        {
            using(var scope = scopeFactory.Get())
            {
                var entity = scope.Query(new Logic.Appointment.GetAppointmentById(model.Id));
                entity.StartDate = model.NewDate.AddHours(model.NewStartHour);
                entity.EndDate = model.NewDate.AddHours(model.NewEndHour).AddMinutes(59).AddSeconds(59);

                scope.Execute(new Logic.Appointment.UpdateAppointment(entity));

                scope.Commit();
            }

            return View();
        }

        public ActionResult GetForWeek(string id)
        {
            DateTime startDay = DateTime.Parse(id);
            var appointments = scopeFactory.Do(x => x.Query(new Logic.Appointment.GetAppointmentsForUserAndWeek(User.Id, startDay)));

            return Json(appointments.Select(x => new ViewModels.Appointments.Appointment
            {
                id = x.Id,
                name = x.Task.Name,
                taskId = x.Task.Id,
                startDate = x.StartDate.ToString("yyyy-MM-dd"),
                startHour = x.StartDate.Hour,
                endDate = x.EndDate.ToString("yyyy-MM-dd"),
                endHour = x.EndDate.Hour,
                progress = x.Progress,
                duration = x.EndDate.Hour - x.StartDate.Hour + 1
            }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Remove(int id)
        {
            scopeFactory.Do(x => x.Execute(new Logic.Appointment.DeleteAppointment(id)));

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            var entity = scopeFactory.Do(x => x.Query(new Logic.Appointment.GetAppointmentById(id)));
            ViewData.Model = new ViewModels.Appointments.Edit
            {
                AvailableDates = CalendarHelper.GetWeekDays(entity.StartDate.Date).AsNamed(x => string.Format("{0:yyyy-MM-dd} ({1})", x, x.DayOfWeek)),
                AvailableHours = Enumerable.Range(7, 12).ToArray(),

                Id = entity.Id,
                Date = entity.StartDate.Date,
                StartHour = entity.StartDate.Hour,
                EndHour = entity.EndDate.Hour + 1
            };

            return PartialView();
        }

        [HttpPost]
        public ActionResult Edit(ViewModels.Appointments.Edit model)
        {
            if(model.EndHour <= model.StartHour)
            {
                ModelState.AddModelError("EndHour", "Cannot finish before start");
            }

            var startDate = model.Date.AddHours(model.StartHour);
            var endDate = model.Date.AddHours(model.EndHour).AddSeconds(-1);

            if(scopeFactory.Do(x => x.Query(new Logic.Appointment.IsAnyOverlapping(startDate, endDate, model.Id, User.Id))))
            {
                ModelState.AddModelError("StartHour", "Appointment overlaps with another");
                ModelState.AddModelError("EndHour", "Appointment overlaps with another");
            }

            if(ModelState.IsValid)
            {
                using(var scope = scopeFactory.Get())
                {
                    var entity = scope.Query(new Logic.Appointment.GetAppointmentById(model.Id));

                    entity.StartDate = model.Date.AddHours(model.StartHour);
                    entity.EndDate = model.Date.AddHours(model.EndHour).AddSeconds(-1);

                    scope.Execute(new Logic.Appointment.UpdateAppointment(entity));

                    scope.Commit();

                    return Json(new ViewModels.Appointments.Appointment
                    {
                        id = entity.Id,
                        name = entity.Task.Name,
                        taskId = entity.Task.Id,
                        startDate = entity.StartDate.ToString("yyyy-MM-dd"),
                        startHour = entity.StartDate.Hour,
                        endDate = entity.EndDate.ToString("yyyy-MM-dd"),
                        endHour = entity.EndDate.Hour,
                        progress = entity.Progress,
                        duration = entity.EndDate.Hour - entity.StartDate.Hour + 1
                    });
                }
            }
            else
            {
                model.AvailableDates = CalendarHelper.GetWeekDays(model.Date.Date).AsNamed(x => string.Format("{0:yyyy-MM-dd} ({1})", x, x.DayOfWeek));
                model.AvailableHours = Enumerable.Range(7, 12).ToArray();

                ViewData.Model = model;

                Response.StatusCode = 550;
                return PartialView();
            }
        }

        public ActionResult UpcomingAppointments()
        {
            var now = DateTime.Now.TranslateToTimeZone(User.TimeZone);
            var data = scopeFactory.Do(x => x.Query(new Logic.Appointment.GetAppointmentsForUserAndDay(User.Id, now)));
            ViewData.Model = new ViewModels.Appointments.UpcomingAppointments(data, now);
            return View();
        }
    }
}
