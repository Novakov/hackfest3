﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;

namespace Web.Controllers
{
    public class ProjectController : ControllerBase
    {
        private IScopeFactory scopeFactory;

        public ProjectController(IScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        public ActionResult Create()
        {
            ViewData.Model = new Model.Project();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Model.Project model)
        {
            if (ModelState.IsValid)
            {
                if (User != null)
                {
                    model.User = User;
                }
                else
                {
                    throw new InvalidOperationException("You must be logged in to create projects.");
                }

                model.Closed = false;
                int id = scopeFactory.Do(x => x.Execute(new Logic.Project.CreateProject(model)));

                return Json("OK");
            }

            ViewData.Model = model;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewData.Model = scopeFactory.Do(x => x.Query(new Logic.Project.GetProjectById(id)));
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Model.Project model)
        {
            if (ModelState.IsValid)
            {
                if (model.User.Id != User.Id)
                {
                    throw new InvalidOperationException("You can't edit projects which aren't yours.");
                }

                model.User = User;
                scopeFactory.Do(x => x.Execute(new Logic.Project.UpdateProject(model)));

                return Json("OK");
            }

            ViewData.Model = model;
            return View();
        }

        public ActionResult Delete(int id)
        {
            scopeFactory.Do(x => x.Execute(new Logic.Project.DeleteProject(id)));

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}
