﻿/// <reference path="jquery-1.7.1.js" />

$(document).ready(function () {
    $("#generate").click(generateLink);

    /*var token = $.get(rootPath + 'Account/GetToken');
    if (token != "Null") {
        $("#link").html(rootPath + 'Account/GetiCal/' + token);
        $("#link").attr('href', rootPath + 'Account/GetiCal/' + token);
    }*/
});

function generateLink() {
    var destEl = $("#link");

    $.getJSON(rootPath + 'Account/GenerateNewToken', function (token) {
        destEl.html(token.link);
        destEl.attr('href', token.link);
    });
}