﻿/// <reference path="jquery-1.7.1.js" />
/// <reference path="misc.js" />

var calendarArray = [];

function loadCalendar(url) {
    window.history.replaceState('Object', 'Calendar', url);

    $('#calendar').load(url, function () {                
        setupWeekLinks();
        setupCalendar();
        loadAppointments();

        $('#jump-target').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#jump').click(jumpDateClicked);
    });
}

function switchWeekClick() {
    var element = $(this);

    var url = element.attr('href');

    loadCalendar(url);

    return false;
}

function setupWeekLinks() {
    $('.calendar-week .prev a').click(switchWeekClick);
    $('.calendar-week .next a').click(switchWeekClick);
}

function reloadCalendar() {
    var cal = $('.calendar');
    var year = cal.data('current-year');
    var month = cal.data('current-month');
    var day = cal.data('current-day');

    var url = rootPath + 'Calendar/' + year + '/' + month + '/' + day;

    $('#calendar').load(url, function () {
        setupWeekLinks();
        setupCalendar();
        loadAppointments();
    });
}

function setupCalendar() {
    $('.calendar .cell').droppable({
        drop: function (event, ui) {
            if(!(ui.draggable.hasClass('busy-cell') || ui.draggable.hasClass('task')))
            {
                return;
            }

            var duration = ui.draggable.data('duration');

            var hoveredCell = $(this);

            var topHour = hoveredCell.data('hour-index') - Math.floor((duration / 2));
            var lastHour = topHour + duration - 1;
            var day = hoveredCell.data('day');

            if (validatePlace(topHour, duration, day)) {
                for (i = topHour + 1; i <= lastHour; i++) {
                    var cell = calendarArray[i][day];
                    cell.element.css('display', 'none');

                    cell.occupied = true;
                }

                var appointment = {
                    id: 0,
                    name: ui.draggable.html(),
                    taskId: ui.draggable.data('task-id'),
                    startDate: hoveredCell.data('date'),
                    startHour: hoveredCell.data('hour') - Math.floor((duration / 2)),
                    endHour: hoveredCell.data('hour') - Math.floor((duration / 2)) + duration - 1,
                    progress: 0,
                    duration: duration
                };

                if (ui.draggable.hasClass('busy-cell')) {
                    var appId = ui.draggable.data('app-id');
                    calendarArray[topHour][day].element.attr('data-app-id', appId);
                    recreateCells(ui.draggable);

                    var updateData = {
                        id: appId,
                        newDate: hoveredCell.data('date'),
                        newStartHour: hoveredCell.data('hour') - Math.floor((duration / 2)),
                        newEndHour: hoveredCell.data('hour') - Math.floor((duration / 2)) + duration - 1
                    };

                    $.post(rootPath + 'Appointments/Update', updateData);

                    appointment.id = appId;
                    placeAppointment(appointment);
                } else {
                    var newApp = {
                        taskId: ui.draggable.data('task-id'),
                        date: hoveredCell.data('date'),
                        startHour: hoveredCell.data('hour') - Math.floor((duration / 2)),
                        endHour: hoveredCell.data('hour') - Math.floor((duration / 2)) + duration - 1
                    };

                    $.post(rootPath + 'Appointments/Create', newApp, function (result) {
                        appointment.id = result.id;

                        placeAppointment(appointment);
                    });
                }
            }
        }
    });

    for (hour = 0; hour <= 10; hour++) {
        calendarArray[hour] = [];
        for (day = 0; day <= 6; day++) {
            var id = "#cell-" + hour + "-" + day;
            var element = $(id);
            calendarArray[hour][day] = {
                id: id,
                hour: element.data('hour'),
                day: day,
                element: element,
                occupied: false,
                date: element.data('date')
            };
        }
    }
}

function recreateCells(topCell) {
    topCell
        .html('&nbsp;')
        .removeClass('busy-cell')
        .removeAttr('rowspan')
        .removeAttr('data-task-id');

    var duration = topCell.data('duration');
    var topIndex = topCell.data('hour-index');
    var day = topCell.data('day');
    var lastIndex = topIndex + duration - 1;

    calendarArray[topIndex][day].occupied = false;

    for (i = topIndex + 1; i <= lastIndex; i++) {
        calendarArray[i][day].element.css('display', 'table-cell');
        calendarArray[i][day].occupied = false;
    }
}

function validatePlace(topHour, duration, day) {       
    if(topHour < 0) {
        return false;
    }
 
    if((topHour + duration - 1) > 10) {
        return false;
    }

    for (i = topHour; i < topHour + duration; i++) {
        if (calendarArray[i][day].occupied) {
            return false;
        }
    }

    return true;
}

function taskDragStart(event, ui) {
    var duration = $(this).data('duration')    
    ui.helper
        .width(70)
        .height(duration * 40);                   
}

function loadTasks() {
    $.getJSON(rootPath + 'Task/GetAvailable', function (data) {
        $('#tasks').html('');
        for (var i in data) {
            task = data[i];

            var d = $('<div class="task"></div>')
                .html(task.name)
                .attr('data-duration', task.duration)
                .attr('data-task-id', task.id)
                .css('height', task.duration * 39)
                .appendTo('#tasks');
        }

        $('#tasks .task').draggable({
            revert: true,
            revertDuration: 0,
            helper: "clone"
        });
    });
}

function loadAppointments() {
    $.getJSON(rootPath + 'Appointments/GetForWeek/' + calendarArray[0][0].element.data('date'), function (data) {
        for (var i in data) {
            placeAppointment(data[i]);
        }
    });
}

function placeAppointment(appointment) {
    var topCell = $('td[data-date=' + appointment.startDate + '][data-hour=' + appointment.startHour + ']');
    var topHour = topCell.data('hour-index');
    var lastCell = $('td[data-hour-index=' + (topHour + appointment.duration - 1) + ']');
    var lastHour = lastCell.data('hour-index');
    var day = topCell.data('day');

    calendarArray[topHour][day].occupied = true;

    for (i = topHour + 1; i <= lastHour; i++) {
        var cell = calendarArray[i][day];
        cell.element.css('display', 'none');

        cell.occupied = true;
    }

    calendarArray[topHour][day].element
            .css('display', 'table-cell')
            .attr('rowspan', appointment.duration)
            .addClass('busy-cell')
            .html(appointment.name)
            .attr('data-duration', appointment.duration)
            .attr('data-task-id', appointment.taskId)
            .attr('data-app-id', appointment.id)
            .draggable({
                revert: true,
                revertDuration: 0,
                helper: "clone",
                start: taskDragStart,
                appendTo: $("#calendar")
            });            
}

function removeAppointment(cell) {
    confirmDelete({
        title: 'Remove appointment',
        text: 'Are you sure to remove this appointment?',
        remove: function() {
            $.getJSON(rootPath + 'Appointments/Remove/' + cell.data('app-id'));
            recreateCells(cell);
        }
    });
}

function editAppointment(cell) {
    var id = cell.data('app-id');
    $('#editAppointment').dialog({
        title: 'Edit appointment',
        modal: true,
        width: 350,
        height: 300,
        open: function() {
            $(this).load(rootPath + 'Appointments/Edit/' + id);
        }
    });
}

function appointmentUpdated(data) {    
    $('#editAppointment').dialog('close');

    var appCell = $('td[data-app-id=' + data.id + ']');
    recreateCells(appCell);

    placeAppointment(data);
}

function editAppointmentFailed(xhr) {
    $('#editAppointment').html(xhr.responseText);
}

function createTaskClicked() {
      $('#dialog').dialog({
        title: 'Create new task',
        width: 350,
        height: 300,
        modal: true,
        open: function() {
            $(this).load(rootPath + 'Task/Create');
        }
      });

      return false;
}

function taskCreated() {
    $('#dialog').dialog('close');
    loadTasks();
}

function taskCreationFailed(xhr, errorType, ex) {
    $('#dialog').html(xhr.responseText);
}

function editTask(taskId) {
    $('#dialog').dialog({
        title: 'Edit task',
        width: 350,
        height: 300,
        modal: true,
        open: function() {
            $(this).load(rootPath + 'Task/Edit/' + taskId);
        }
    });
}

function taskUpdated() {
    $('#dialog').dialog('close');
    loadTasks();
    reloadCalendar();
}

function taskUpdateFailed(xhr, errorType, ex) {
    $('#dialog').html(xhr.responseText);
}

function removeTask(taskId) {
    confirmDelete({
        title: 'Remove task',
        text: 'Are you sure to remove this task? <br /> Appointments associated with it will not be removed',
        remove: function() {   
            $.getJSON(rootPath + 'Task/Delete/' + taskId, function() {
                loadTasks();
            });
        }
    });
}

function jumpDateClicked() {
    var date = $('#jump-target').datepicker('getDate');

    var url = rootPath + 'Calendar/' + date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
    
    loadCalendar(url);

    return false;
}

$(document).ready(function () {
    setupWeekLinks();

    setupCalendar();

    loadTasks();

    loadAppointments();

    $.contextMenu({
        selector: '.busy-cell',
        items: {            
            'edit': {
                name: 'Edit',
                className: 'context-edit',
                callback: function () {
                    editAppointment($(this));
                }
            },
            'remove': {
                name: 'Remove',
                className: 'context-remove',
                callback: function () {
                    removeAppointment($(this));
                }
            }         
        },        
    });

    $.contextMenu({
        selector: '.task',
        items: {            
            'edit': {
                name: 'Edit',
                className: 'context-edit',
                callback: function () {
                    editTask($(this).data('task-id'));
                }
            },
            'remove': {
                name: 'Remove',
                className: 'context-remove',
                callback: function () {
                    removeTask($(this).data('task-id'));
                }
            }         
        },        
    });

    $('#create-task').click(createTaskClicked);   
    
    $('#jump-target').datepicker({
            dateFormat: 'yy-mm-dd'
    }); 
    $('#jump').click(jumpDateClicked);
});