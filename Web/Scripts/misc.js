﻿/// <reference path="jquery-1.7.1.js" />

function confirmDelete(options) {
    if (options == undefined) {
        options = {};
    }

    $('<div></div>').dialog({
        title: options.title,
        modal: true,
        width: 400,
        height: 150,

        open: function () {
            $(this).html(options.text);

            var dialog = $(this);

            $('.ui-widget-overlay').click(function (e) {
                dialog.dialog('close');
            });
        },

        buttons: {
            "Remove": function () {
                $(this).dialog('close');
                if (options.remove != undefined) {
                    options.remove();
                }
            },

            "Cancel": function () {
                $(this).dialog('close');
                if (options.cancel != undefined) {
                    options.cancel();
                }
            }
        }
    });
}

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
    return this;
}

$(document).ready(function () {
    $('#loading-box')
        .center();

    $('#loading').ajaxStart(function () {
        $(this).show(0);
    });

    $('#loading').ajaxComplete(function () {
        $(this).hide(0);
    });
});