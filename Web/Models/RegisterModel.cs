﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Display name is required")]
        [Display(Name = "Display name")]
        public string DisplayName
        {
            get;
            set;
        }

        [Required(ErrorMessage = "E-mail address is required")]
        [Display(Name = "E-mail")]
        public string Email
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "Timezone")]
        public string TimeZone
        {
            get;
            set;
        }

        public string OpenID
        {
            get;
            set;
        }
    }
}