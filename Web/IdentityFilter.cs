﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Logic;
using System.Security.Principal;
using DataAccess;

namespace Web
{
    public class IdentityFilter : IAuthorizationFilter
    {
        #region IAuthorizationFilter Members

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if(filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                if(!( filterContext.HttpContext.User.Identity is UserIdentity ))
                {
                    filterContext.HttpContext.User = UpdateToUserIdentity(filterContext.HttpContext.User);
                }
            }
        }

        private IPrincipal UpdateToUserIdentity(IPrincipal principal)
        {
            var scopeFactory = DependencyResolver.Current.GetService<IScopeFactory>();

            using(var scope = scopeFactory.Get())
            {
                var user = scope.Query(new Logic.User.GetByOpenId(principal.Identity.Name));

                var identity = new Logic.UserIdentity(user);

                return new GenericPrincipal(identity, new string[0]);
            }
        }

        #endregion
    }
}