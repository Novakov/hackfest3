﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.Calendar
{
    public class View
    {       
        public DateTime[] Days
        {
            get;
            set;
        }

        public int[] Hours
        {
            get;
            set;
        }

        public Common.WeekRange Week
        {
            get;
            set;
        }

        public string PrevWeekUrl
        {
            get;
            set;
        }

        public string NextWeekUrl
        {
            get;
            set;
        }
    }
}