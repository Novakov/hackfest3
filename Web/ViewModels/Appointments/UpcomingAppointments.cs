﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.Appointments
{
    public class UpcomingAppointments
    {
        public UpcomingAppointments(IEnumerable<Model.Appointment> data, DateTime now)
        {
            if (data.Count() > 0)
            {
                HasAppointments = true;
                TimeInAppointments = data.Select(x => (x.EndDate.AddSeconds(1) - x.StartDate)).Aggregate((y, z) => y + z).TotalHours;
                if (now >= data.ElementAt(0).StartDate && now <= data.ElementAt(0).EndDate)
                {
                    var upcomingData = new List<Model.Appointment>(data);
                    upcomingData.RemoveAt(0);
                    NextAppointments = upcomingData;
                    CurrentAppointment = data.ElementAt(0);
                }
                else
                {
                    NextAppointments = new List<Model.Appointment>(data);
                    CurrentAppointment = null;
                }
            }
            else
            {
                HasAppointments = false;
                TimeInAppointments = 0;
                NextAppointments = new List<Model.Appointment>();
                CurrentAppointment = null;
            }
        }

        public Model.Appointment CurrentAppointment
        {
            get;
            set;
        }

        public IEnumerable<Model.Appointment> NextAppointments
        {
            get;
            set;
        }

        public double TimeInAppointments
        {
            get;
            set;
        }

        public bool HasAppointments
        {
            get;
            set;
        }
    }
}