﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.Appointments
{
    public class Appointment
    {
        public int id
        {
            get;
            set;
        }

        public string name
        {
            get;
            set;
        }

        public int taskId
        {
            get;
            set;
        }

        public string startDate
        {
            get;
            set;
        }

        public int startHour
        {
            get;
            set;
        }

        public string endDate
        {
            get;
            set;
        }

        public int endHour
        {
            get;
            set;
        }

        public int duration
        {
            get;
            set;
        }

        public float progress
        {
            get;
            set;
        }
    }
}