﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.Appointments
{
    public class Edit
    {
        public DateTime Date
        {
            get;
            set;
        }

        [Display(Name = "Start hour")]
        public int StartHour
        {
            get;
            set;
        }

        [Display(Name = "End hour")]
        public int EndHour
        {
            get;
            set;
        }

        public IEnumerable<Helpers.NamedDateTime> AvailableDates
        {
            get;
            set;
        }

        public int[] AvailableHours
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }
    }
}