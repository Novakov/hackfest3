﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.Appointments
{
    public class Update
    {
        public int Id
        {
            get;
            set;
        }

        public DateTime NewDate
        {
            get;
            set;
        }
        
        public int NewStartHour
        {
            get;
            set;
        }

        public int NewEndHour
        {
            get;
            set;
        }
    }
}