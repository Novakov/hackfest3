﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.Appointments
{
    public class Create
    {
        public int TaskId
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public int StartHour
        {
            get;
            set;
        }

        public int EndHour
        {
            get;
            set;
        }
    }
}