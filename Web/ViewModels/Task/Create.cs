﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.Task
{
    public class Create
    {
        public Model.Task Task
        {
            get;
            set;
        }

        public Tuple<TimeSpan, string>[] AvailableDurations
        {
            get;
            set;
        }
    }
}