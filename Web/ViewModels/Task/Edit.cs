﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.Task
{
    public class Edit
    {
        public int Id
        {
            get;
            set;
        }

        [Required]
        public string Name
        {
            get;
            set;
        }

        [Required]
        public string Description
        {
            get;
            set;
        }

        [Required]
        public TimeSpan Duration
        {
            get;
            set;
        }

        public bool Visible
        {
            get;
            set;
        }

        public Tuple<TimeSpan, string>[] AvailableDurations
        {
            get;
            set;
        }
    }
}