﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.User
{
    public class GetUserByCalendarToken : IQuery<Model.User>
    {
        private string calendarToken;

        public GetUserByCalendarToken(string calendarToken)
        {
            this.calendarToken = calendarToken;
        }

        public Model.User Execute(NHibernate.ISession session)
        {
            try
            {
                return session.QueryOver<Model.User>()
                    .Where(x => x.CalendarToken == calendarToken)
                    .List().First();
            }
            catch
            {
                throw new NullReferenceException();
            }
        }
    }
}
