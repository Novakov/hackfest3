﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.User
{
    public class RegisterUser : DataAccess.Crud.CreateCommand<Model.User, int>
    {
        private Model.User user;

        public RegisterUser(string email, string displayName, string token, string timezone)
        {
            this.user = new Model.User
            {
                Mail = email,
                Name = displayName,
                Token = token,
                TimeZone = timezone
            };
        }

        public override Model.User GetEntity()
        {
            return this.user;
        }
    }
}
