﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.User
{
    public class UpdateUser : DataAccess.Crud.UpdateCommand<Model.User, object>
    {
        private Model.User user;

        public UpdateUser(Model.User user)
        {
            this.user = user;
        }

        public override Model.User GetEntity()
        {
            return this.user;
        }
    }
}
