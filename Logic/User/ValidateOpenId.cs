﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.User
{
    public class ValidateOpenId : IQuery<bool>
    {
        private string token;
        public ValidateOpenId(string token)
        {
            this.token = token;
        }

        #region IQuery<bool> Members

        public bool Execute(NHibernate.ISession session)
        {
            var q = session.QueryOver<Model.User>()
                    .Where(x => x.Token == token)
                    .RowCount();

            return q == 1;
        }

        #endregion
    }
}
