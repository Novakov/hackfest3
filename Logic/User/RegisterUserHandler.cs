﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.User
{
    public class RegisterUserHandler : DataAccess.Crud.CreateCommandHandler<RegisterUser, Model.User, int>
    {
        protected override void OnAfterSave(Model.User entity)
        {
            Result = entity.Id;
        }
    }
}
