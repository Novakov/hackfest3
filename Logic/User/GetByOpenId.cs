﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.User
{
    public class GetByOpenId : IQuery<Model.User>
    {
        private string token;

        public GetByOpenId(string token)
        {
            this.token = token;
        }

        #region IQuery<User> Members

        public Model.User Execute(NHibernate.ISession session)
        {
            return session.QueryOver<Model.User>()
                   .Where(x => x.Token == token)
                   .SingleOrDefault();            
        }

        #endregion
    }
}
