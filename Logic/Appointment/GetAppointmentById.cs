﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Appointment
{
    public class GetAppointmentById : DataAccess.Crud.GetById<Model.Appointment>
    {
        public GetAppointmentById(int id)
            : base(id)
        {

        }
    }
}
