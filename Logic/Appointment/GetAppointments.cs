﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Appointment
{
    public class GetAppointments : DataAccess.Crud.GetAll<Model.Appointment>
    {
        protected override NHibernate.IQueryOver<Model.Appointment, Model.Appointment> Extend(NHibernate.IQueryOver<Model.Appointment, Model.Appointment> query)
        {
            return query.Fetch(x => x.Task).Eager;
        }
    }
}
