﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.Appointment
{
    public class IsAnyOverlapping : IQuery<bool>
    {
        private DateTime endDate;
        private DateTime startDate;
        private int appointmentId;
        private int userId;

        public IsAnyOverlapping(DateTime startDate, DateTime endDate, int appointmentId, int userId)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.appointmentId = appointmentId;
            this.userId = userId;
        }

        #region IQuery<Appointment> Members

        public bool Execute(NHibernate.ISession session)
        {
            Model.Task task = null;
            Model.User user = null;

            var q = session.QueryOver<Model.Appointment>()
                    .Inner.JoinAlias(x => x.Task, () => task)
                    .Inner.JoinAlias(() => task.User, () => user, ()=>user.Id == userId)
                    .Where(x => x.Id != appointmentId)
                    .AndNot(x => ( endDate < x.StartDate )
                              || ( x.EndDate < startDate ))
                    .RowCount();

            return q > 0;
        }

        #endregion
    }
}
