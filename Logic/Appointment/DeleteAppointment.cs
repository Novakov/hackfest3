﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Crud;

namespace Logic.Appointment
{
    public class DeleteAppointment : DeleteCommand<Model.Appointment, object>
    {
        public DeleteAppointment(int id) :
            base(id)
        {
        }
    }
}
