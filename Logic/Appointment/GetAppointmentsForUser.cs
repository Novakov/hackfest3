﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.Appointment
{
    public class GetAppointmentsForUser : IQuery<IEnumerable<Model.Appointment>>
    {
        private int userId;

        public GetAppointmentsForUser(int userId)
        {
            this.userId = userId;
        }

        public IEnumerable<Model.Appointment> Execute(NHibernate.ISession session)
        {
            Model.Appointment app = null;
            Model.Task task = null;
            Model.User user = null;

            return session.QueryOver<Model.Appointment>(() => app)
                .JoinAlias(x => x.Task, () => task)
                .Inner.JoinAlias(x => task.User, () => user, () => (user.Id == userId))
                .List();
        }
    }
}
