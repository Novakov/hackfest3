﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Crud;

namespace Logic.Appointment
{
    public class CreateAppointment : CreateCommand<Model.Appointment, int>
    {
        private Model.Appointment appointment;

        public CreateAppointment(Model.Appointment appointment)
        {
            this.appointment = appointment;
        }

        public override Model.Appointment GetEntity()
        {
            return this.appointment;
        }
    }
}
