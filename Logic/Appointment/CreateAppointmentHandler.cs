﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Appointment
{
    public class CreateAppointmentHandler : DataAccess.Crud.CreateCommandHandler<CreateAppointment, Model.Appointment, int>
    {
        protected override void OnAfterSave(Model.Appointment entity)
        {
            Result = entity.Id;
        }
    }
}
