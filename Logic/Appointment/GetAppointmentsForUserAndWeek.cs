﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.Appointment
{
    public class GetAppointmentsForUserAndWeek : IQuery<IEnumerable<Model.Appointment>>
    {
        private int userId;
        private DateTime startDay;

        public GetAppointmentsForUserAndWeek(int userId, DateTime startDay)
        {
            this.userId = userId;
            this.startDay = startDay;
        }

        public IEnumerable<Model.Appointment> Execute(NHibernate.ISession session)
        {
            Model.Appointment app = null;
            Model.Task task = null;
            Model.User user = null;

            var weekRange = Common.CalendarHelper.GetWeekRange(startDay);
            weekRange.EndDate.Add(new TimeSpan(23, 59, 59));

            return session.QueryOver<Model.Appointment>(() => app)
                .JoinAlias(x => x.Task, () => task)
                .Inner.JoinAlias(x => task.User, () => user, () => (user.Id == userId))
                .Where(x => ((weekRange.StartDate <= x.StartDate) && (x.StartDate <= weekRange.EndDate)))
                .List();
        }
    }
}
