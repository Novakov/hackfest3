﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Appointment
{
    public class UpdateAppointment : DataAccess.Crud.UpdateCommand<Model.Appointment, object>
    {
        private Model.Appointment appointment;

        public UpdateAppointment(Model.Appointment appointment)
        {
            this.appointment = appointment;
        }

        public override Model.Appointment GetEntity()
        {
            return this.appointment;
        }
    }
}
