﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.Appointment
{
    public class GetAppointmentsForUserAndDay : IQuery<IEnumerable<Model.Appointment>>
    {
        private int userId;
        private DateTime start;

        public GetAppointmentsForUserAndDay(int userId, DateTime start)
        {
            this.userId = userId;
            this.start = start;
        }

        public IEnumerable<Model.Appointment> Execute(NHibernate.ISession session)
        {
            Model.Appointment app = null;
            Model.Task task = null;
            Model.User user = null;

            DateTime end = start.AddDays(0); //make a copy
            end = end.AddDays(1);

            return session.QueryOver<Model.Appointment>(() => app)
                .JoinAlias(x => x.Task, () => task)
                .Inner.JoinAlias(x => task.User, () => user, () => (user.Id == userId))
                .Where(x => (((end > x.StartDate) && (x.StartDate > start)) || ((x.EndDate > start) && (x.EndDate < end))))
                .OrderBy(x => x.StartDate).Asc
                .List();
        }
    }
}
