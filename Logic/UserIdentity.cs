﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

namespace Logic
{
    public class UserIdentity : IIdentity
    {
        public UserIdentity(Model.User user)
        {
            this.User = user;
        }

        #region IIdentity Members

        public string AuthenticationType
        {
            get
            {
                return "OpenId";
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return true;
            }
        }

        public string Name
        {
            get
            {
                return User.Name;
            }
        }

        #endregion

        public Model.User User
        {
            get;
            private set;
        }
    }
}
