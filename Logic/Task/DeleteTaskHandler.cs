﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Task
{
    public class DeleteTaskHandler : DataAccess.StoreCommandHandler<DeleteTask, object>
    {
        protected override void Execute(NHibernate.ISession session, DeleteTask command)
        {
            var entity = session.Load<Model.Task>(command.Id);

            entity.Visible = false;

            session.Update(entity);
        }
    }
}
