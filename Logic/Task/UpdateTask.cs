﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Task
{
    public class UpdateTask : DataAccess.Crud.UpdateCommand<Model.Task, object>
    {
        private Model.Task task;

        public UpdateTask(Model.Task task)
        {
            this.task = task;
        }

        public override Model.Task GetEntity()
        {
            return this.task;
        }
    }
}
