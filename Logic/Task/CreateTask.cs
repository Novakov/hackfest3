﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Crud;

namespace Logic.Task
{
    public class CreateTask : CreateCommand<Model.Task, int>
    {
        private Model.Task task;

        public CreateTask(Model.Task task)
        {
            this.task = task;
        }

        public override Model.Task GetEntity()
        {
            return this.task;
        }
    }
}
