﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Task
{
    public class GetTaskById : DataAccess.Crud.GetById<Model.Task>
    {
        public GetTaskById(int id)
            : base(id)
        {

        }
    }
}
