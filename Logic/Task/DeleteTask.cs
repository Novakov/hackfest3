﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Crud;

namespace Logic.Task
{
    public class DeleteTask : DeleteCommand<Model.Task, object>
    {
        public DeleteTask(int id) :
            base(id)
        {
        }
    }
}
