﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Task
{
    public class CreateTaskHandler : DataAccess.Crud.CreateCommandHandler<CreateTask, Model.Task, int>
    {
        protected override void OnAfterSave(Model.Task entity)
        {
            Result = entity.Id;
        }
    }
}
