﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Logic.Task
{
    public class GetTasksForUser : IQuery<IEnumerable<Model.Task>>
    {
        private int userId;

        public GetTasksForUser(int userId)
        {
            this.userId = userId;
        }

        #region IQuery<IEnumerable<Task>> Members

        public IEnumerable<Model.Task> Execute(NHibernate.ISession session)
        {
            return session.QueryOver<Model.Task>()
                    .Where(x => ( x.User.Id == userId ) && ( x.Visible ))
                    .List();
        }

        #endregion
    }
}
