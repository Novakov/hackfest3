﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Crud;

namespace Logic.Project
{
    public class DeleteProject : DeleteCommand<Model.Project, object>
    {
        public DeleteProject(int id) :
            base(id)
        {
        }
    }
}
