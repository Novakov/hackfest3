﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Project
{
    public class CreateProjectHandler : DataAccess.Crud.CreateCommandHandler<CreateProject, Model.Project, int>
    {
        protected override void OnAfterSave(Model.Project entity)
        {
            Result = entity.Id;
        }
    }
}
