﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Project
{
    public class GetProjectById : DataAccess.Crud.GetById<Model.Project>
    {
        public GetProjectById(int id)
            : base(id)
        {

        }
    }
}
