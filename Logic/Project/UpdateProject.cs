﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Project
{
    public class UpdateProject : DataAccess.Crud.UpdateCommand<Model.Project, object>
    {
        private Model.Project project;

        public UpdateProject(Model.Project project)
        {
            this.project = project;
        }

        public override Model.Project GetEntity()
        {
            return this.project;
        }
    }
}
