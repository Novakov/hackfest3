﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Project
{
    public class DeleteProjectHandler : DataAccess.StoreCommandHandler<DeleteProject, object>
    {
        protected override void Execute(NHibernate.ISession session, DeleteProject command)
        {
            var entity = session.Load<Model.Project>(command.Id);

            entity.Closed = true;

            session.Update(entity);
        }
    }
}
