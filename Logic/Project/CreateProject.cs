﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Crud;

namespace Logic.Project
{
    public class CreateProject : CreateCommand<Model.Project, int>
    {
        private Model.Project project;

        public CreateProject(Model.Project project)
        {
            this.project = project;
        }

        public override Model.Project GetEntity()
        {
            return this.project;
        }
    }
}
