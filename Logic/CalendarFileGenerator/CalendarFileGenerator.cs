﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDay;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using Logic.Appointment;
using DataAccess;


namespace Logic.CalendarFileGenerator
{
    public class CalendarFileGenerator
    {
        private int userId;

        public CalendarFileGenerator(int userId)
        {
            this.userId = userId;
        }

        public string GetICalString(IScopeFactory scopeFactory)
        {
            iCalendar iCal = new iCalendar();

            var collection = scopeFactory.Do(x => x.Query(new Logic.Appointment.GetAppointmentsForUser(userId)));

            foreach (Model.Appointment app in collection)
            {
                Event e = iCal.Create<Event>();

                e.Start = new iCalDateTime(app.StartDate);
                e.End = new iCalDateTime(app.EndDate);
                e.Summary = app.Task.Name;
                e.Description = app.Task.Description;
                e.Location = "Location";
                e.Created = new iCalDateTime(DateTime.Now);
            }

            iCalendarSerializer serializer = new iCalendarSerializer(iCal);
            string ret = serializer.SerializeToString();            

            return ret;
        }
    }
}
