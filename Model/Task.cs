﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Task : Entity
    {
        [Required]
        public virtual string Name
        {
            get;
            set;
        }

        [Required]
        public virtual string Description
        {
            get;
            set;
        }

        public virtual bool IsReusable
        {
            get;
            set;
        }

        public virtual bool Visible
        {
            get;
            set;
        }

        [Required]
        public virtual TimeSpan Duration
        {
            get;
            set;
        }

        public virtual IList<Appointment> Appointments
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }

        public virtual Project Project
        {
            get;
            set;
        }
    }
}
