﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Appointment : Entity
    {

        public virtual DateTime StartDate
        {
            get;
            set;
        }

        public virtual DateTime EndDate
        {
            get;
            set;
        }

        public virtual float Progress
        {
            get;
            set;
        }

        public virtual Task Task
        {
            get;
            set;
        }
    }
}
