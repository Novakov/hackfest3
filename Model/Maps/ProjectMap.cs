﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Maps
{
    public class ProjectMap : EntityMap<Project>
    {
        public ProjectMap()
        {
            Map(x => x.Name);
            Map(x => x.Description);
            Map(x => x.Closed);
            
            HasMany<Task>(x => x.Tasks)
                .LazyLoad();
            
            References(x => x.User)
                .LazyLoad();            
        }
    }
}
