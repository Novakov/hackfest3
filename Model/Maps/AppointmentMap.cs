﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Maps
{
    public class AppointmentMap : EntityMap<Appointment>
    {
        public AppointmentMap()
        {
            Map(x => x.StartDate, "startDateUtc");
            Map(x => x.EndDate, "endDateUtc");
            Map(x => x.Progress);
            
            References(x => x.Task)
                .LazyLoad();            
        }
    }
}
