﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Maps
{
    public class TaskMap : EntityMap<Task>
    {
        public TaskMap()
        {
            Map(x => x.Name);
            Map(x => x.Description);
            Map(x => x.IsReusable);
            Map(x => x.Visible);
            Map(x => x.Duration);
            HasMany<Appointment>(x => x.Appointments)
                    .LazyLoad();
            
            References(x => x.User)
                .LazyLoad();

            References(x => x.Project)
                .LazyLoad();
        }
    }
}
