﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Maps
{
    public class UserMap : EntityMap<User>
    {
        public UserMap()
        {
            Map(x => x.Mail);
            Map(x => x.Name);
            Map(x => x.TimeZone);
            Map(x => x.Token);
            Map(x => x.CalendarToken);
            
            HasMany<Project>(x => x.Projects)
                .LazyLoad();

            HasMany<Task>(x => x.Tasks)
                .LazyLoad();           
        }
    }
}
