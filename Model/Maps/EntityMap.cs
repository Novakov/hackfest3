﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Model.Maps
{
    public abstract class EntityMap<TEntity> : ClassMap<TEntity>
        where TEntity : Entity
    {
        public EntityMap()
        {
            Id(x => x.Id);
        }
    }
}
