﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class User : Entity
    {
        [Required]
        [Display(Name="Display name")]
        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Token
        {
            get;
            set;
        }

        [Required]
        [Display(Name="Time zone")]
        public virtual string TimeZone
        {
            get;
            set;
        }

        [Required]
        [Display(Name="E-mail")]
        public virtual string Mail
        {
            get;
            set;
        }

        public virtual string CalendarToken
        {
            get;
            set;
        }

        public virtual IList<Project> Projects
        {
            get;
            set;
        }

        public virtual IList<Task> Tasks
        {
            get;
            set;
        }
    }
}
