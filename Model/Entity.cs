﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public abstract class Entity : Common.IIdentitable
    {
        #region IIdentitable Members

        public virtual int Id
        {
            get;
            set;
        }

        #endregion
    }
}
