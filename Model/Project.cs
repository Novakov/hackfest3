﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Project : Entity
    {
        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Description
        {
            get;
            set;
        }

        public virtual bool Closed
        {
            get;
            set;
        }

        public virtual IList<Task> Tasks
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }
    }
}
