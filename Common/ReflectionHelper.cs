﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;

namespace Common
{
    public static class ReflectionHelper
    {
        public static MethodInfo GetMethod<TType>(Expression<Action<TType>> method)
        {
            var memberAccess = method.Body as MethodCallExpression;
            if((memberAccess == null) || !(memberAccess.Method is MethodInfo))
            {
                throw new ArgumentOutOfRangeException("Not pointing to method");
            }            

            return memberAccess.Method;
        }
    }
}
