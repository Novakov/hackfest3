﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Common
{
    public struct WeekRange
    {
        public DateTime StartDate
        {
            get;
            set;
        }
        public DateTime EndDate
        {
            get;
            set;
        }
    }

    public static class CalendarHelper
    {
        private static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;

            return GetFirstDayOfWeek(dayInWeek, defaultCultureInfo);
        }

        private static DateTime GetFirstDayOfWeek(DateTime dayInWeek, CultureInfo cultureInfo)
        {
            //DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek firstDay = DayOfWeek.Monday;
            DateTime firstDayInWeek = dayInWeek.Date;
            while(firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }

        public static WeekRange GetWeekRange(DateTime date)
        {
            WeekRange wr = new WeekRange();
            wr.StartDate = GetFirstDayOfWeek(date);
            wr.EndDate = wr.StartDate.AddDays(7);

            return wr;
        }

        public static List<DateTime> GetWeekDays(DateTime date)
        {
            List<DateTime> list = new List<DateTime>();

            DateTime dt = GetFirstDayOfWeek(date);

            for(int i = 0; i < 7; i++)
            {
                list.Add(dt);
                dt = dt.AddDays(1);
            }

            return list;
        }
    }
}
