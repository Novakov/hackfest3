﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Reflect = System.Reflection;
using NHibernate;
using FluentNHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;

namespace DataAccess.DI
{
    public class NHibernateModule : Module
    {
        private Reflect.Assembly[] domain;
        private string connectionString;

        public NHibernateModule(string connectionString, params Reflect.Assembly[] domain)
        {
            this.connectionString = connectionString;
            this.domain = domain;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(BuildSessionFactory).As<ISessionFactory>().SingleInstance();

            builder.RegisterType<NH.ScopeFactory>().As<IScopeFactory>().InstancePerLifetimeScope();
        }

        private ISessionFactory BuildSessionFactory(IComponentContext ctx)
        {
            return
            Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
                .Mappings(ConfigureMappings)
                .BuildSessionFactory();
        }

        private void ConfigureMappings(MappingConfiguration cfg)
        {
            foreach(var item in domain)
            {
                cfg.FluentMappings.AddFromAssembly(item);
            }

            cfg.FluentMappings.Conventions.Add(Table.Is(x => x.EntityType.Name + "s"));
            cfg.FluentMappings.Conventions.Add(ForeignKey.EndsWith("Id"));
        }
    }
}
