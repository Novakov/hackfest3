﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Reflect = System.Reflection;

namespace DataAccess.DI
{
    public class CqrsModule : Module
    {
        private Reflect.Assembly[] commands;

        public CqrsModule(params Reflect.Assembly[] commands)
        {
            this.commands = commands;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(commands)
                .AsClosedTypesOf(typeof(ICommandHandler<,>))
                .AsImplementedInterfaces();
        }
    }
}
