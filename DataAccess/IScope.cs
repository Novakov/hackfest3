﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public interface IScope : IDisposable
    {
        void Commit();
        TResult Execute<TResult>(ICommand<TResult> command);
        TResult Query<TResult>(IQuery<TResult> query);
    }
}
