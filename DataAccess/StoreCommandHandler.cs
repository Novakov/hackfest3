﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace DataAccess
{
    public abstract class StoreCommandHandler<TCommand, TResult> : ICommandHandler<TCommand, TResult>
        where TCommand : ICommand<TResult>
    {
        #region ICommandHandler<TCommand,TResult> Members

        public TResult Result
        {
            get;
            protected set;
        }

        protected IScope Scope
        {
            get;
            private set;
        }

        public void Execute(IScope scope, TCommand command)
        {
            Scope = scope;
            this.Execute(( scope as ISessionProvider ).Session, command);
        }

        #endregion

        protected abstract void Execute(ISession session, TCommand command);
    }
}
