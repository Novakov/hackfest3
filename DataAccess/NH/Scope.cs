﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using Autofac;
using System.Reflection;
using Common;

namespace DataAccess.NH
{
    class Scope : IScope, ISessionProvider
    {
        private static MethodInfo executeCommand = ReflectionHelper.GetMethod<Scope>(x => x.ExecuteCommand<ICommand<object>, object>(null)).GetGenericMethodDefinition();

        private ISession session;
        private ITransaction transaction;
        private ILifetimeScope lifetimeScope;

        public Scope(ISession session, ILifetimeScope lifetimeScope)
        {            
            this.session = session;
            this.lifetimeScope = lifetimeScope;

            this.transaction = session.BeginTransaction();
        }

        #region IScope Members

        public void Commit()
        {
            transaction.Commit();
        }

        public TResult Execute<TResult>(ICommand<TResult> command)
        {
            return (TResult) executeCommand.MakeGenericMethod(command.GetType(), typeof(TResult)).Invoke(this, new[] { command });
        }

        public TResult Query<TResult>(IQuery<TResult> query)
        {
            return query.Execute(this.session);
        }        

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            lifetimeScope.Dispose();

            if(transaction.IsActive)
            {
                transaction.Rollback();
            }

            session.Dispose();

            ScopeFactory.currentScope = null;
        }

        #endregion

        #region ISessionProvider Members

        public ISession Session
        {
            get
            {
                return this.session;
            }
        }

        #endregion

        private TResult ExecuteCommand<TCommand, TResult>(TCommand command)
            where TCommand : ICommand<TResult>
        {
            var handler = lifetimeScope.Resolve<ICommandHandler<TCommand, TResult>>();
            handler.Execute(this, command);

            return handler.Result;
        }
    }
}
