﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using Autofac;

namespace DataAccess.NH
{
    class ScopeFactory : IScopeFactory
    {
        [ThreadStatic]
        internal static Scope currentScope = null;

        private ISessionFactory sessionFactory;
        private ILifetimeScope lifetimeScope;

        public ScopeFactory(ISessionFactory sessionFactory, ILifetimeScope lifetimeScope)
        {
            this.sessionFactory = sessionFactory;
            this.lifetimeScope = lifetimeScope;
        }

        #region IScopeFactory Members

        public IScope Get()
        {
            if(currentScope == null)
            {
                currentScope = new Scope(sessionFactory.OpenSession(), lifetimeScope.BeginLifetimeScope());

                return currentScope;
            }
            else
            {
                return new ProxyScope(currentScope);
            }            
        }

        #endregion
    }
}
