﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.NH
{
    class ProxyScope : IScope, ISessionProvider
    {
        private Scope scope;

        public ProxyScope(Scope scope)
        {            
            this.scope = scope;
        }

        #region IScope Members

        public void Commit()
        {
            // Nothing to do
        }

        public TResult Execute<TResult>(ICommand<TResult> command)
        {
            return scope.Execute<TResult>(command);
        }

        public TResult Query<TResult>(IQuery<TResult> query)
        {
            return scope.Query<TResult>(query);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            // Nothing to do
        }

        #endregion

        #region ISessionProvider Members

        public NHibernate.ISession Session
        {
            get
            {
                return this.Session;
            }
        }

        #endregion
    }
}
