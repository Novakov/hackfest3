﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace DataAccess
{
    public interface IQuery<TResult>
    {
        TResult Execute(ISession session);
    }
}
