﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Crud
{
    public abstract class GetById<TEntity> : IQuery<TEntity>
    {
        private int id;

        public GetById(int id)
        {
            this.id = id;
        }
        
        #region IQuery<TEntity> Members

        public TEntity Execute(NHibernate.ISession session)
        {
            return session.Get<TEntity>(id);
        }

        #endregion
    }
}
