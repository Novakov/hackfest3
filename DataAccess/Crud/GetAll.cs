﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace DataAccess.Crud
{
    public abstract class GetAll<TEntity> : IQuery<IEnumerable<TEntity>>
        where TEntity : class
    {
        #region IQuery<IEnumerable<TEntity>> Members

        public IEnumerable<TEntity> Execute(NHibernate.ISession session)
        {
            var query = session.QueryOver<TEntity>();

            return Extend(query).List();
        }

        protected virtual IQueryOver<TEntity, TEntity> Extend(NHibernate.IQueryOver<TEntity, TEntity> query)
        {
            return query;
        }

        #endregion
    }
}
