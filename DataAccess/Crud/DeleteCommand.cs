﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Crud
{
    public abstract class DeleteCommand<TEntity, TResult> : ICommand<TResult>
    {
        public int Id
        {
            get;
            protected set;
        }

        public DeleteCommand(int id)            
        {
            this.Id = id;
        }
    }
}
