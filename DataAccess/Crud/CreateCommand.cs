﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Crud
{
    public abstract class CreateCommand<TEntity, TResult>
         : ICommand<TResult>
    {
        public abstract TEntity GetEntity();
    }
}
