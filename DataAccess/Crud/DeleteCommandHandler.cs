﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Crud
{
    public class DeleteCommandHandler<TCommand, TEntity, TResult> : StoreCommandHandler<TCommand, TResult>
        where TCommand : DeleteCommand<TEntity, TResult>
    {
        protected override void Execute(NHibernate.ISession session, TCommand command)
        {
            var entity = session.Load<TEntity>(command.Id);
            
            OnBeforeDelete(entity);

            session.Delete(entity);

            OnAfterDelete(entity);
        }

        protected virtual void OnBeforeDelete(TEntity entity)
        {
            
        }
        protected virtual void OnAfterDelete(TEntity entity)
        {

        }
    }
}
