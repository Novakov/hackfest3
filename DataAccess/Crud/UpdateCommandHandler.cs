﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Crud
{
    public abstract class UpdateCommandHandler<TCommand, TEntity, TResult> : StoreCommandHandler<TCommand, TResult>
        where TCommand : UpdateCommand<TEntity, TResult>
    {
        protected override void Execute(NHibernate.ISession session, TCommand command)
        {
            TEntity entity = command.GetEntity();

            OnBeforeUpdate(entity);

            session.Update(entity);

            OnAfterUpdate(entity);
        }

        protected virtual void OnAfterUpdate(TEntity entity)
        {
            
        }

        protected virtual void OnBeforeUpdate(TEntity entity)
        {
            
        }
    }
}
