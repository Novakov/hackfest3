﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Crud
{
    public abstract class CreateCommandHandler<TCommand, TEntity, TResult> : StoreCommandHandler<TCommand, TResult>
        where TCommand : CreateCommand<TEntity, TResult>
    {
        protected override void Execute(NHibernate.ISession session, TCommand command)
        {            
            var entity = command.GetEntity();
         
            OnBeforeSave(entity);
            
            session.Save(entity);

            OnAfterSave(entity);
        }

        protected virtual void OnBeforeSave(TEntity entity)
        {
        }

        protected virtual void OnAfterSave(TEntity entity)
        {
        }
    }
}
