﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public static class ScopeFactoryExtensions
    {
        public static TResult Do<TResult>(this IScopeFactory @this, Func<IScope, TResult> action)
        {
            using(var scope = @this.Get())
            {
                var result = action(scope);

                scope.Commit();

                return result;
            }
        }       
    }
}
